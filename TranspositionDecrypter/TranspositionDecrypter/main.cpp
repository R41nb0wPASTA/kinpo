#include <QCoreApplication>
#include <QTextCodec>
#include <iostream>
#include <vector>
#include <string>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <cstring>
#include <locale.h>


using namespace std;

/*
    Функция разбиения строки на части
    param[in] UndividedString - строка для разделения
    param[in|out] PartsOfString - вектор для записи разделённой строки
    param[in] Direction - направление разделения (0 - слева направо; 1 - справа налево)
    param[in] PartLength - длина отделяемой группы строки
*/
void StringPartsDivision(QString UndividedString, QVector<QString>& PartsOfString, bool Direction, int PartLength)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    // разделение из конца в начало
    if(Direction)
    {
         int k = 0;
         for(int i=UndividedString.length(); i>0; i-=PartLength)
         {
             // обработка частей строки, не выходящих размером за границу самой строки
             int j=i;
             if(j-PartLength+1 > 0)
             {
                 QString tempString = UndividedString.mid(j-PartLength, PartLength);
                 PartsOfString.push_back(tempString);
             }
             // обработка частей строки, выходящих размером за границу самой строки
             else
             {
                 QString tempString = UndividedString.mid(0,UndividedString.length()-k);
                 PartsOfString.push_back(tempString);
             }
             k+=PartLength;
         }
    }
    // разделение из начала в конец
    else
    {
         int k = 0;
         for(int i=0; i<UndividedString.length(); i+=PartLength)
         {
             // обработка частей строки, не выходящих размером за границу самой строки
             int j=i;
             if(j+PartLength-1 <= UndividedString.length())
             {
                 QString tempString = UndividedString.mid(i, PartLength);
                 PartsOfString.push_back(tempString);
             }
             // обработка частей строки, выходящих размером за границу самой строки
             else
             {
                 QString tempString = UndividedString.mid(k,UndividedString.length()-k+1);
                 PartsOfString.push_back(tempString);
             }
             k+=PartLength;
         }
    }
}

/*
    Функция расшифровки строки
    param[in] UndecrypterdString - строка для расшифровки
    param[in|out] Result - строка для записи расшифровки
*/
void StringTranspositionDecryption(QString UndecryptedString, QString &Result)
{
    QVector<QString> strDivision;

    // разделить строку на части
    QString rowTemp = UndecryptedString.mid(0,UndecryptedString.length()-2);
    StringPartsDivision(rowTemp, strDivision, 1, (((int)UndecryptedString.toStdString().back())-95));

    // склеить строку из полученных частей
    QString tempStr;
    for(int k=0; k<strDivision.size(); k++)
    {
        tempStr.append(strDivision[k]);
    }

    // сохранение результата работы функции (расшифрованной строки)
    Result = tempStr;
}

int main(int argc, char *argv[])
{

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));

    QCoreApplication a(argc, argv);

    // вектор для записи строк из входного файла
    QVector<QString> rows;

    // ввод пути к файлу
    cout << QString::fromUtf8("Введите абсолютный путь до зашифрованного текста:").toLocal8Bit().data() << endl;

    // валидность входного файла
    bool isValid = true;
    bool doesExist = true;

    string tempPath;
    cin >> tempPath;
    cout << endl;

    QString path = QString::fromStdString(tempPath);

    QFile inputFile(path);

    // СЧИТЫВАНИЕ СТРОК ИЗ ФАЙЛА
    // если входной файл существует
    // считаем входной файл валидным
    if(inputFile.exists())
    {
        inputFile.open(QIODevice::ReadOnly | QIODevice::Text);

        QTextStream in(&inputFile);
        in.setCodec(QTextCodec::codecForName("UTF-8"));

        // поочерёдно считываем каждую строку текста
        int i = 1;
        while (!in.atEnd())
        {
           QString line = in.readLine();
           rows.push_back(line);

           // если считана строка с длиной 2 для вывода соответствующей ошибки
           if(line.length() == 2)
           {
                // если считана пустрая строка с символом N
                if(isalpha(line.toStdString().back()) && iswspace(line.toStdString().at(line.toStdString().length() - 2)))
                {
                    cout << QString::fromUtf8("Ввод некорректен: пустую строку ").toLocal8Bit().data() << i << QString::fromUtf8(" невозможно расшифровать").toLocal8Bit().data() << endl;
                    isValid = false;
                }
                // если считана строка без символа N
                else
                {
                    cout << QString::fromUtf8("Ввод некорректен: в строке ").toLocal8Bit().data() << i << QString::fromUtf8(" отсутствует описывающий группу символов символ N").toLocal8Bit().data() << endl;
                    isValid = false;
                }
           }
           // если считана строка с длиной 1 для вывода соответствующей ошибки
           else if(line.length() == 1)
           {
                // если введена строка без символа N
                cout << QString::fromUtf8("Ввод некорректен: в строке ").toLocal8Bit().data() << i << QString::fromUtf8(" отсутствует описывающий группу символов символ N").toLocal8Bit().data() << endl;
                isValid = false;
           }
           // если считана строка с длиной больше 2 для вывода соответствующей ошибки
           else if (!(isalpha(line.toStdString().back()) && iswspace(line.toStdString().at(line.toStdString().length() - 2)))) {
               // если введена строка без символа N
               cout << QString::fromUtf8("Ввод некорректен: в строке ").toLocal8Bit().data() << i << QString::fromUtf8(" отсутствует описывающий группу символов символ N").toLocal8Bit().data() << endl;
               isValid = false;
           }
           i++;
        }
        inputFile.close();
    }
    // если входной файл не существует
    else
    {
        cout << QString::fromUtf8("Указанный файл не существует!").toLocal8Bit().data() << endl;
        isValid = false;
        doesExist = false;
    }

    QVector<QString> result;

    // РАСШИФРОВКА СЧИТАННЫХ ИЗ ВХОДНОГО ФАЙЛА СТРОК
    // если входной файл соответствует правилам ввода
    if(isValid)
    {
        // создание файла вывода расшифровки
        QFile fileOut("out.txt");
            fileOut.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out(&fileOut);
            out.setCodec(QTextCodec::codecForName("UTF-8"));

        // расшифровка считанных из входного файла строк
        for(int j = 0; j < rows.size(); j++)
        {
            // расшифровка каждой строки поочерёдно
            QString tempStr;
            StringTranspositionDecryption(rows[j], tempStr);

            // запись расшифрованной строки в файл вывода расшифровки
            out << QString::fromStdString(tempStr.toStdString()) << endl;
        }

        QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
        cout << QString::fromUtf8("Текст расшифрован! Файл с результатом работы находится в корневой папке программы (out.txt)").toLocal8Bit().data() << endl;
    }
    // если входной файл не соответствует правилам ввода
    else
    {
        if(doesExist){
            cout << endl << QString::fromUtf8("Зашифрованный текст содержит ошибки!").toLocal8Bit().data() << endl;
        }
    }

    //cin.get();

    return a.exec();
}
