#include <QCoreApplication>
#include <QTextCodec>
#include <iostream>
#include <vector>
#include <string>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <cstring>
#include <locale.h>

#include "tst_divanddec.h"

#include <gtest/gtest.h>

using namespace std;

/*
    Функция разбиения строки на части
    param[in] UndividedString - строка для разделения
    param[in|out] PartsOfString - вектор для записи разделённой строки
    param[in] Direction - направление разделения (0 - слева направо; 1 - справа налево)
    param[in] PartLength - длина отделяемой группы строки
*/
void StringPartsDivision(QString UndividedString, QVector<QString>& PartsOfString, bool Direction, int PartLength)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    // разделение из конца в начало
    if(Direction)
    {
         int k = 0;
         for(int i=UndividedString.length(); i>0; i-=PartLength)
         {
             // обработка частей строки, не выходящих размером за границу самой строки
             int j=i;
             if(j-PartLength+1 > 0)
             {
                 QString tempString = UndividedString.mid(j-PartLength, PartLength);
                 PartsOfString.push_back(tempString);
             }
             // обработка частей строки, выходящих размером за границу самой строки
             else
             {
                 QString tempString = UndividedString.mid(0,UndividedString.length()-k);
                 PartsOfString.push_back(tempString);
             }
             k+=PartLength;
         }
    }
    // разделение из начала в конец
    else
    {
         int k = 0;
         for(int i=0; i<UndividedString.length(); i+=PartLength)
         {
             // обработка частей строки, не выходящих размером за границу самой строки
             int j=i;
             if(j+PartLength-1 <= UndividedString.length())
             {
                 QString tempString = UndividedString.mid(i, PartLength);
                 PartsOfString.push_back(tempString);
             }
             // обработка частей строки, выходящих размером за границу самой строки
             else
             {
                 QString tempString = UndividedString.mid(k,UndividedString.length()-k+1);
                 PartsOfString.push_back(tempString);
             }
             k+=PartLength;
         }
    }
}

/*
    Функция расшифровки строки
    param[in] UndecrypterdString - строка для расшифровки
    param[in|out] Result - строка для записи расшифровки
*/
void StringTranspositionDecryption(QString UndecryptedString, QString &Result)
{
    QVector<QString> strDivision;

    // разделить строку на части
    QString rowTemp = UndecryptedString.mid(0,UndecryptedString.length()-2);
    StringPartsDivision(rowTemp, strDivision, 1, (((int)UndecryptedString.toStdString().back())-95));

    // склеить строку из полученных частей
    QString tempStr;
    for(int k=0; k<strDivision.size(); k++)
    {
        tempStr.append(strDivision[k]);
    }

    // сохранение результата работы функции (расшифрованной строки)
    Result = tempStr;
}


TEST(StringPartsDivisionTests, CorrectDivision)
{
    //abcdef 0,1 2      
    //строка делится на равные части минимальной заданой длины справа налево
    QVector<QString> test1;
    QVector<QString> expected_answer1{"ef","cd","ab"};
    StringPartsDivision("abcdef",test1,1,2);
    EXPECT_TRUE(1 == qEqual(test1.begin(), test1.end(), expected_answer1.begin()));

    //строка делится на равные части минимальной заданой длины слева направо
    QVector<QString> test2;
    QVector<QString> expected_answer2{"ab","cd","ef"};
    StringPartsDivision("abcdef",test2,0,2);
    EXPECT_TRUE(1 == qEqual(test2.begin(), test2.end(), expected_answer2.begin()));

    //abcdefg 0,1 2     
    //строка делится на неравные части справа налево
    QVector<QString> test5;
    QVector<QString> expected_answer5{"fg","de","bc","a"};
    StringPartsDivision("abcdefg",test5,1,2);
    EXPECT_TRUE(1 == qEqual(test5.begin(), test5.end(), expected_answer5.begin()));

    //строка делится на неравные части слева направо
    QVector<QString> test6;
    QVector<QString> expected_answer6{"ab","cd","ef","g"};
    StringPartsDivision("abcdefg",test6,0,2);
    EXPECT_TRUE(1 == qEqual(test6.begin(), test6.end(), expected_answer6.begin()));

    //abcdef 0,1 6      
    //строка делится ровно на 1 часть (длина отделяемой группы символов равна длине строки) слева направо
    QVector<QString> test7;
    QVector<QString> expected_answer7{"abcdef"};
    StringPartsDivision("abcdef",test7,0,6);
    EXPECT_TRUE(1 == qEqual(test7.begin(), test7.end(), expected_answer7.begin()));

    //строка делится ровно на 1 часть (длина отделяемой группы символов равна длине строки) справа налево
    QVector<QString> test8;
    QVector<QString> expected_answer8{"abcdef"};
    StringPartsDivision("abcdef",test8,1,6);
    EXPECT_TRUE(1 == qEqual(test8.begin(), test8.end(), expected_answer8.begin()));

    //abcdef 0,1 7      
    //строка делится группы символов с длиной больше чем длина самой строки слева направо
    QVector<QString> test9;
    QVector<QString> expected_answer9{"abcdef"};
    StringPartsDivision("abcdef",test9,0,7);
    EXPECT_TRUE(1 == qEqual(test9.begin(), test9.end(), expected_answer9.begin()));

    //строка делится группы символов с длиной больше чем длина самой строки справа налево
    QVector<QString> test10;
    QVector<QString> expected_answer10{"abcdef"};
    StringPartsDivision("abcdef",test10,1,7);
    EXPECT_TRUE(1 == qEqual(test10.begin(), test10.end(), expected_answer10.begin()));
}

TEST(StringTranspositionDecryption, CorrectDecryption)
{
    //efcdab a (min 2) -> abcdef        расшифровка строки с минимальной длиной группы символов
    QString res1;
    StringTranspositionDecryption("efcdab a", res1);
    EXPECT_TRUE("abcdef" == res1);

    //defabc b (3) -> abcdef        расшифровка строки с не минимальной длиной группы символов (меньше длины строки)
    QString res2;
    StringTranspositionDecryption("defabc b", res2);
    EXPECT_TRUE("abcdef" == res2);

    //abcdef e (6) -> abcdef        расшифровка строки с длиной группы символов равной длине строки
    QString res3;
    StringTranspositionDecryption("abcdef e", res3);
    EXPECT_TRUE("abcdef" == res3);

    //abcdef f (7) -> abcdef        расшифровка строки с длиной группы символов больше длины строки
    QString res4;
    StringTranspositionDecryption("abcdef f", res4);
    EXPECT_TRUE("abcdef" == res4);

    //abcdef z (max 27) -> abcdef       расшифровка строки с максимальной длиной группы символов
    QString res5;
    StringTranspositionDecryption("abcdef z", res5);
    EXPECT_TRUE("abcdef" == res5);

    //ef  ab a (min 2 ws) -> abcdef     расшифровка строки с белыми разделителями
    QString res6;
    StringTranspositionDecryption("ef  ab a", res6);
    EXPECT_TRUE("ab  ef" == res6);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
